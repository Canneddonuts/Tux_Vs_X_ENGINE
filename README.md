# Tux Vs X ENGINE

A remake of the first game I made.
<p>This time made with C99 and raylib.</p>

<p>Sprites made by Jelly_poi.</p>
<p>Programming and design by M-C-O-B.</p>

Make sure to read the LICENSE file


# TO-DO

* More clean code
* Build Guide
* Screen Boundaries
* Projectiles
* Portable Binaries of the game

# BRANCHES

Use
```sh
git checkout unstable
```
to see the unstable branch
