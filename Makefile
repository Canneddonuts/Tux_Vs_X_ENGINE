#COPYRIGHT 2021 by M-C-O-B all code is under the GNU General Public License v3.0 and later

#files to compile

OBJS = src/Main.c src/Controls.c src/Debug.c src/Copying.c

#compiler to use

CC = cc

#gimmie ALL dem 1999 era warnings yo

COMPILER_FLAGS = -std=c99 -Wall

#libs we linkin bro

LINKER_FLAGS = -lraylib -lGL -lm -lpthread -ldl -lrt -lX11

#name of muh bin lel

OBJ_NAME = T_V_X_E

#no regerts cuz its all together now

all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
