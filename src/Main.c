#include <stdbool.h>
#include "raylib.h"

#include "Controls.h"
#include "Debug.h"
#include "Copying.h"

// Functions


// Globles

// Position Varaibles
float EnemyPos_x = 482.0f, EnemyPos_y = 62.0f;
float TuxPos_x = 32.0f, TuxPos_y = 62.0f;

// Toggle bools
bool ShowHitbox = false;
bool ToggleShowPos = false;

// Setup Code
void Setup()
{
  const int screenWidth = 800;
  const int screenHeight = 450;

  InitWindow(screenWidth, screenHeight, "Tux Vs Windows");

  SetTargetFPS(60);
}

// GameLoop Code
void MainGameLoop()
{
  // Load Textures
  Texture2D tux = LoadTexture("assets/tux.png");
  Texture2D enemy = LoadTexture("assets/enemy.png");
  Texture2D arctic = LoadTexture("assets/arctic.png");

  // Hitboxes
  Rectangle EnemyHitbox = { GetScreenWidth()/2.0f - 30, GetScreenHeight()/2.0f - 30, 290, 300};
  Rectangle TuxHitbox = { GetScreenWidth()/2.0f - 30, GetScreenHeight()/2.0f - 30, 90, 40};


  bool collision = false;

    // GameLoop
    while (!WindowShouldClose()) {

          // Checks and calls the Pos show function
          if (IsKeyPressed(KEY_P)) ToggleShowPos = !ToggleShowPos;
          if (ToggleShowPos==true) ShowPos();

          Controls();

          TuxHitbox.x = TuxPos_x+50;
          TuxHitbox.y = TuxPos_y+50;
          EnemyHitbox.x = EnemyPos_x;
          EnemyHitbox.y = EnemyPos_y;

          collision = CheckCollisionRecs(EnemyHitbox, TuxHitbox);

      BeginDrawing();
        ClearBackground(RAYWHITE);

        // Put the sprites and text on the screen
        DrawTexture(arctic, 50, 0, RAYWHITE);
        DrawTexture(tux, TuxPos_x, TuxPos_y, RAYWHITE);
        DrawTexture(enemy, EnemyPos_x, EnemyPos_y, RAYWHITE);

        CheckHitboxToggle();

        if (ShowHitbox==true) {
          DrawRectangleRec(EnemyHitbox, RED);
          DrawRectangleRec(TuxHitbox, BLUE);
        }

        if (collision) {
          DrawText("FINALY BASIC AWFUL COLLISION", 50, 18, 20, MAROON);
        } else {
          DrawText("Tux Vs X ENGINE PROTOTYPE please read the LINCENSE", 50, 18, 20, BLACK);
        }

        DrawText("Press 'H' to toggle hitboxes", 50, 72, 20, DARKGREEN);
        DrawText("Press 'P' to toggle printing of the positions", 50, 95, 20, DARKGREEN);

      EndDrawing();
    }
  // Unload Textures
  UnloadTexture(tux);
  UnloadTexture(enemy);
  UnloadTexture(arctic);
}

int main()
{
  Setup();
  MainGameLoop();
  CloseWindow();
  License();

  return 0;
}
