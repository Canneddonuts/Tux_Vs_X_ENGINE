#include <stdio.h>
#include <stdbool.h>
#include "raylib.h"

#include "Main.h"
#include "Debug.h"

void ShowPos()
{
  // Ouput Position Infomation in the command line
  printf
  (
    "TuxPos_x = %f TuxPos_y = %f EnemyPos_x = %f EnemyPos_y = %f\n",
     TuxPos_x, TuxPos_y, EnemyPos_x, EnemyPos_y
  );
}

void CheckHitboxToggle()
{
  if (IsKeyPressed(KEY_H)) ShowHitbox = !ShowHitbox;
}
