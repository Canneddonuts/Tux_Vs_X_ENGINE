#include <stdio.h>


void License()
{
  // Outputs Copying info in the command line
  printf
  (
    "\nTux Vs X Engine  Copyright (C) 2021  Canneddonuts Team.\n"
    "This program comes with ABSOLUTELY NO WARRANTY; \n"
    "This is free software, and you are welcome to redistribute it\n"
    "under certain conditions; for details read the LICENSE.\n"
    "For the source code go to https://gitdab.com/Canneddonuts/Tux_Vs_X_ENGINE.git\n"
  );
}
