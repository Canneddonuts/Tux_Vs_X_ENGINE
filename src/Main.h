#include <stdbool.h>
#pragma once

// makes the Varaibles globle across c files
extern float TuxPos_x;
extern float TuxPos_y;
extern float EnemyPos_x;
extern float EnemyPos_y;

extern bool ShowHitbox;
extern bool ToggleShowPos;
