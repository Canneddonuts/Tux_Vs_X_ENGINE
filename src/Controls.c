#include "raylib.h"

#include "Main.h"
#include "Controls.h"

// Controls
void Controls()
{
  if (IsKeyDown(KEY_LEFT)) TuxPos_x -= GetFrameTime() * 800.0f;
  if (IsKeyDown(KEY_RIGHT)) TuxPos_x += GetFrameTime() * 800.0f;
  if (IsKeyDown(KEY_UP)) TuxPos_y -= GetFrameTime() * 800.0f;
  if (IsKeyDown(KEY_DOWN)) TuxPos_y += GetFrameTime() * 800.0f;
}
